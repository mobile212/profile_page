import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Color.fromRGBO(186, 255, 255, 50),
        iconTheme: IconThemeData(
          color: Colors.black54,
        ),
      ),
      dividerTheme: DividerThemeData(
          color: Colors.black54
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Color.fromRGBO(186, 255, 255,1),
        foregroundColor: Colors.black54
      ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.blueGrey,
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      dividerTheme: DividerThemeData(
        color: Colors.white54
      ),

    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}


class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
        ? MyAppTheme.appThemeLight()
        : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBar(),
        body: buildListView(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.light_mode),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.call,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Call"),
      ],
    );
  }

  Widget buildTextButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.textsms_outlined,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Text"),
      ],
    );
  }

  Widget buildVideoButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.video_call,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Video"),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.email_rounded,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Email"),
      ],
    );
  }

  Widget buildDirectionButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.directions,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Direction"),
      ],
    );
  }

  Widget buildPayButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.attach_money,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Pay"),
      ],
    );
  }

  Widget mobilePhoneLiistTile() {
    return ListTile(
      leading: Icon(Icons.call),
      title: Text("999-999-9999"),
      subtitle: Text("Mobile"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        color: Colors.blueAccent.shade400,
        onPressed: () {},
      ),
    );
  }

  Widget otherPhoneLiistTile() {
    return ListTile(
      leading: Text(""),
      title: Text("999-000-0000"),
      subtitle: Text("other"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        color: Colors.blueAccent.shade400,
        onPressed: () {},
      ),
    );
  }

  Widget emailLiistTile() {
    return ListTile(
      leading: Icon(Icons.email),
      title: Text("arona01@kivo.bl.ac"),
      subtitle: Text("other"),
    );
  }

  Widget directLiistTile() {
    return ListTile(
      leading: Icon(Icons.location_on),
      title: Text("999-000-0000"),
      subtitle: Text("234 Kivo st, Burlin"),
      trailing: IconButton(
        icon: Icon(Icons.directions),
        color: Colors.blueAccent.shade400,
        onPressed: () {},
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      //backgroundColor: Color.fromRGBO(186, 255, 255, 50),
      leading: Icon(Icons.home),
      actions: <Widget>[
        IconButton(onPressed: () {}, icon: Icon(Icons.people)),
      ],
      //iconTheme: IconThemeData(
        //color: Colors.black54,
      //),
    );
  }

  Widget buildListView() {
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 250,
              child: Image.network(
                "https://wallpapers.com/images/file/arona-blue-archive-crz0up0wq91benyq.jpg",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text(
                      "Arona",
                      style: TextStyle(
                          fontSize: 32,
                          fontFamily: 'Cheri',
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              //color: Colors.black54,
            ),
            Container(
                margin: const EdgeInsets.only(top: 2, bottom: 5),
                child: Theme(
                  data: ThemeData(
                      iconTheme: IconThemeData(
                    color: Colors.blueAccent.shade400,
                  )),
                  child: profileActionItems(),
                )),
            Divider(
              //color: Colors.black54,
            ),
            mobilePhoneLiistTile(),
            otherPhoneLiistTile(),
            emailLiistTile(),
            directLiistTile(),
          ],
        ),
      ],
    );
  }

  Widget profileActionItems() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildCallButton(),
        buildTextButton(),
        buildVideoButton(),
        buildEmailButton(),
        buildDirectionButton(),
        buildPayButton(),
      ],
    );
  }
}
